#include <sys/prctl.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <cap-ng.h>

int main(int argc, char **argv) {
  assert(argc >= 2);
  capng_get_caps_process();
  if(capng_update(CAPNG_ADD, CAPNG_INHERITABLE, CAP_NET_BIND_SERVICE)){
    perror("capng_update CAPNG_ADD CAPNG_INHERITABLE\n");
    exit(1);
  }
  capng_apply(CAPNG_SELECT_CAPS);
  if (prctl(PR_CAP_AMBIENT, PR_CAP_AMBIENT_RAISE, CAP_NET_BIND_SERVICE, 0, 0)) {
    perror("prctl PR_CAP_AMBIENT_RAISE");
    exit(1);
  }

  if (execv(argv[1], argv + 1)){
    perror("exec ");
  }

  return 0;
}
